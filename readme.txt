In this document I want to cover 
1. compilation
2. running tests
3. assumptions and commentary

1. compilation: mvn compile

2. running tests: mvn test

3. assumptions and commentary

- This project requires java 11
- A cart contains a list of cart items.
    A cart item has a product and an item price.
    The product has a unitPrice which is recommended price.  So, the idea
    is that the charged price for the item and the recommended price need not
    be the same.

- I put a default tax rate in Globals.  The idea is
    that there may be a default tax rate that applies.
    Each product also has a tax rate variable which allows us to apply
    products to have varying tax rates as happens in South Africa.
    As an example sugar products are taxed higher than vegetables.
    It may be more efficient to create tasx categories rather than
    assigning tax rates to each product.  This could
    be an improvement for the future.

- I created a bigDecimal factory method to ensure that all 
    my BigDecimals are rounded to 2 decimal places.

- Another possible improvement is to create another abstraction
    which creates a collection of items for each product.
    The usefulness of this expansion could be debated.

