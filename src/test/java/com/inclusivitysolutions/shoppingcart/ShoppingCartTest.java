package com.inclusivitysolutions.shoppingcart;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;
import static com.inclusivitysolutions.shoppingcart.Globals.productCatalogue;

import java.math.BigDecimal;
import java.util.List;

public class ShoppingCartTest {

    @Test
    public void itShouldHave5DoveSoapsAtATotalof199_99AndUnitPriceOf39_99EachHavingStartedWithAnEmptyCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        assertTrue(shoppingCart.getItems().isEmpty());

        shoppingCart.addItem(productCatalogue.get("Dove Soap"), 5);
        assertEquals(5, shoppingCart.getItems().size());
        verifyAllItemsHaveProductName(shoppingCart.getItems(), "Dove Soap");

        verifyThePriceOfEachItemIs(shoppingCart.getItems(), bigDecimal(39.99D));

        assertEquals(bigDecimal("199.95"), shoppingCart.getTotalPriceExcludingTax());
    }

    @Test
    public void itShouldHave8DoveSoapsAtATotalof319_92AndUnitPriceOf39_99EachHavingStartedWith5DoveSoapsInTheCart() {
        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addItem(productCatalogue.get("Dove Soap"), 5);
        assertEquals(5, shoppingCart.getItems().size());

        shoppingCart.addItem(productCatalogue.get("Dove Soap"), 3);
        assertEquals(8, shoppingCart.getItems().size());
        verifyAllItemsHaveProductName(shoppingCart.getItems(), "Dove Soap");

        verifyThePriceOfEachItemIs(shoppingCart.getItems(), bigDecimal(39.99D));

        assertEquals(bigDecimal("319.92"), shoppingCart.getTotalPriceExcludingTax());
    }

    @Test
    public void itShouldHave2DoveSoapsAt39_99EachAnd2AxeDeoAt99_99EachAndTotalTaxIs35_00AndTotalPriceIs314_96() {
        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addItem(productCatalogue.get("Dove Soap"), 2);
        assertEquals(2, shoppingCart.getItems("Dove Soap").size());

        shoppingCart.addItem(productCatalogue.get("Axe Deo"), 2);
        assertEquals(2, shoppingCart.getItems("Axe Deo").size());

        verifyThePriceOfEachItemIs(shoppingCart.getItems("Dove Soap"), bigDecimal(39.99D));
        verifyThePriceOfEachItemIs(shoppingCart.getItems("Axe Deo"), bigDecimal(99.99D));

        assertEquals(bigDecimal("279.96"), shoppingCart.getTotalPriceExcludingTax());
        assertEquals(bigDecimal("35.00"), shoppingCart.getTotalTax());
        assertEquals(bigDecimal("314.96"), shoppingCart.getTotalPrice());
    }

    private void verifyAllItemsHaveProductName(List<CartItem> items, String productName) {
        items.forEach(i -> assertEquals(productName, i.getProduct().getName()));
    }

    private void verifyThePriceOfEachItemIs(List<CartItem> items, BigDecimal price) {
        items.forEach(i -> assertEquals(price, i.getItemPrice()));
    }
}
