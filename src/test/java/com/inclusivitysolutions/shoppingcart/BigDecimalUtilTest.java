package com.inclusivitysolutions.shoppingcart;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import java.math.BigDecimal;

import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;

public class BigDecimalUtilTest {

    @Test
    public void itShouldReturn0_57ForString0_567() {
        assertEquals(new BigDecimal("0.57"), bigDecimal("0.567"));
    }

    @Test
    public void itShouldReturn0_56ForString0_564() {
        assertEquals(new BigDecimal("0.56"), bigDecimal("0.564"));
    }

    @Test
    public void itShouldReturn0_57ForBigDecimal0_567() {
        BigDecimal originalValue = new BigDecimal("0.567");
        assertEquals(new BigDecimal("0.567"), originalValue);
        assertEquals(new BigDecimal("0.57"), bigDecimal(originalValue));
    }

    @Test
    public void itShouldReturn0_56ForBigDecimal0_564() {
        BigDecimal originalValue = new BigDecimal("0.564");
        assertEquals(new BigDecimal("0.564"), originalValue);
        assertEquals(new BigDecimal("0.56"), bigDecimal(originalValue));
    }

    @Test
    public void itShouldReturn0_57ForDouble0_567() {
        assertEquals(new BigDecimal("0.57"), bigDecimal(0.567D));
    }

    @Test
    public void itShouldReturn0_56ForDouble0_564() {
        assertEquals(new BigDecimal("0.56"), bigDecimal(0.564D));
    }
}
