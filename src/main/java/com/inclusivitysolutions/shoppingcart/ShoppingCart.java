package com.inclusivitysolutions.shoppingcart;

import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ShoppingCart {
    private final List<CartItem> cartItems = new ArrayList<CartItem>();

    public ShoppingCart() {
    }

    public ShoppingCart addItem(Product product, int numberOfItems) {
        for (int i=0; i<numberOfItems; i++)
            cartItems.add(new CartItem(product, product.getUnitPrice()));

        return this;
    }

    public List<CartItem> getItems() {
        return cartItems;
    }

    public List<CartItem> getItems(String productName) {
        return cartItems
            .stream()
            .filter(i -> i.getProduct().getName().equals(productName))
            .collect(toList());
    }


    public BigDecimal getTotalTax() {
        return cartItems.stream()
            .map(a -> a.getTax())
            .reduce((a,b) -> a.add(b)
              )
            .map(r -> r)
            .orElseGet(() -> bigDecimal(0D));
    }

    public BigDecimal getTotalPriceExcludingTax() {
        return cartItems.stream()
            .map(a -> a.getItemPrice())
            .reduce((a,b) -> a.add(b)
              )
            .map(r -> r)
            .orElseGet(() -> bigDecimal(0D));
    }

    public BigDecimal getTotalPrice() {
        return getTotalPriceExcludingTax().add(getTotalTax());
    }
}
