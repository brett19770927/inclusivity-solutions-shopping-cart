package com.inclusivitysolutions.shoppingcart;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtil {
    private static BigDecimal applyScaleAndRoundingMode(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal bigDecimal(BigDecimal value) {
        return applyScaleAndRoundingMode(value);
    }

    public static BigDecimal bigDecimal(double value) {
        return applyScaleAndRoundingMode(new BigDecimal(value));
    }

    public static BigDecimal bigDecimal(String value) {
        return applyScaleAndRoundingMode(new BigDecimal(value));
    }
}
