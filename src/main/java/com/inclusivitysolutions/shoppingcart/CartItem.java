package com.inclusivitysolutions.shoppingcart;

import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;

import java.math.BigDecimal;

public class CartItem {
    private Product product;
    private BigDecimal itemPrice;

    public CartItem(Product product, BigDecimal itemPrice) {
        this.product = product;
        this.itemPrice = bigDecimal(itemPrice);
    }

    public Product getProduct() {
        return product;
    }

    public BigDecimal getItemPrice() {
        return this.itemPrice;
    }

    public BigDecimal getTax() {
        return bigDecimal(itemPrice.multiply(product.getTaxRate()));
    }
}
