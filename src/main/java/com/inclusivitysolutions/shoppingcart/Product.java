package com.inclusivitysolutions.shoppingcart;

import java.math.BigDecimal;


import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;

public class Product {
    private String name;
    private BigDecimal unitPrice;
    private BigDecimal taxRate;

    public Product(String name, BigDecimal unitPrice, BigDecimal taxRate) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.taxRate = taxRate;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }
}
