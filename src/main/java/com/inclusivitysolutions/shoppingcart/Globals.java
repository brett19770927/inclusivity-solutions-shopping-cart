package com.inclusivitysolutions.shoppingcart;

import java.math.BigDecimal;
import java.util.Map;

import static java.util.Map.entry;

import static com.inclusivitysolutions.shoppingcart.BigDecimalUtil.bigDecimal;

public class Globals {
    public final static BigDecimal defaultTaxRate = new BigDecimal("0.125");

    public final static Map<String, Product> productCatalogue = Map.ofEntries(
            entry("Dove Soap", new Product(
                    "Dove Soap",
                    bigDecimal(39.99D),
                    defaultTaxRate
                )
              ),
            entry("Axe Deo", new Product(
                    "Axe Deo",
                    bigDecimal(99.99D),
                    defaultTaxRate
                )
              )
        );
}
